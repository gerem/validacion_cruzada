% ScriptRetropropagacion_v20170324
clc;
close all;
clear all;

% El valor es de 0's l?gicos se convierte a -1 por que se utiliza la
% funci?n tangente-hiperb?lica 

X = reshape( textread('dataset.txt', '%f'),170, 100 )';

%Salida deseada
Yd = [
    0 0 0 1 0;
    1 0 0 0 0;
    0 0 0 0 1;
    0 1 0 0 0;
    0 0 0 1 0;
    1 0 0 0 0;
    1 0 0 0 0;
    0 0 0 1 0;
    0 0 0 0 1;
    0 0 1 0 0;
    1 0 0 0 0;
    1 0 0 0 0;
    0 0 0 0 1;
    0 0 0 1 0;
    0 0 1 0 0;
    0 1 0 0 0;
    1 0 0 0 0;
    0 0 1 0 0;
    0 0 0 1 0;
    0 0 0 0 0;
    1 0 0 0 0;
    0 0 0 0 0;
    0 1 0 0 0;
    0 0 1 0 0;
    0 0 0 1 0;
    0 0 0 1 0;
    0 0 1 0 0;
    0 1 0 0 0;
    0 0 0 0 1;
    0 0 0 0 0;
    0 0 1 0 0;
    1 0 0 0 0;
    0 0 0 1 0;
    0 1 0 0 0;
    0 0 1 0 0;
    0 0 0 0 0;
    0 0 0 0 0;
    0 0 0 0 0;
    0 0 1 0 0;
    1 0 0 0 0;
    0 0 1 0 0;
    1 0 0 0 0;
    0 0 0 1 0;
    0 0 1 0 0;
    0 0 0 1 0;
    0 0 0 0 1;
    0 1 0 0 0;
    0 1 0 0 0;
    1 0 0 0 0;
    0 0 0 0 1;
    0 0 1 0 0;
    0 1 0 0 0;
    0 0 1 0 0;
    0 0 0 0 0;
    0 0 0 0 1;
    0 0 0 0 1;
    0 1 0 0 0;
    0 0 0 1 0;
    0 0 0 0 1;
    0 0 0 1 0;
    0 0 1 0 0;
    0 1 0 0 0;
    1 0 0 0 0;
    0 0 0 0 1;
    1 0 0 0 0;
    0 0 0 1 0;
    1 0 0 0 0;
    0 0 0 1 0;
    0 0 1 0 0;
    0 0 0 1 0;
    0 1 0 0 0;
    0 0 0 0 1;
    0 1 0 0 0;
    0 1 0 0 0;
    0 0 1 0 0;
    0 1 0 0 0;
    0 0 0 0 1;
    0 0 0 0 0;
    0 0 0 1 0;
    0 0 1 0 0;
    0 1 0 0 0;
    0 0 0 0 0;
    1 0 0 0 0;
    1 0 0 0 0;
    0 0 1 0 0;
    0 1 0 0 0;
    0 0 0 0 1;
    0 0 1 0 0;
    0 0 0 0 1;
    1 0 0 0 0;
    0 1 0 0 0;
    1 0 0 0 0;
    0 0 0 0 1;
    0 0 0 0 1;
    0 0 0 0 1;
    0 1 0 0 0;
    0 0 0 1 0;
    0 0 0 1 0;
    0 0 0 0 1;
    0 0 0 0 0;
];
    
%definicion de casos de prueba
k1   = [];
yd_1 = [];
p_1  = [];

k2   = [];
yd_2 = [];
p_2  = [];

k3   = [];
yd_3 = [];
p_3  = [];

k4   = [];
yd_4 = [];
p_4  = [];

k5   = [];
yd_5 = [];
p_5  = [];

elements = 100;
k = 5;

size_k = elements / k;
size_p = k - 1;

for i = 0:k - 1
    
    block = ( i * size_k );
    
    pivot = ( size_k * size_p ) - block;
    pivot_origin = elements - block;
    
    curr    =  X( 1:pivot  , 1:end );
    curr_y  = Yd( 1:pivot  , 1:end );
    
    lS = pivot + 1;
    lI = pivot_origin;
    
    fprintf('%1.3f [] %1.3f\n',lS,lI);
    
    curr_pr =  X( lS:lI , 1:end );
    
    if( pivot_origin ~= elements )
        
        for j = 1:block
        
            curr( (pivot + j) , 1:end ) = X( (pivot_origin + j) , 1:end );
            curr_y( (pivot + j) , 1:end ) = Yd( (pivot_origin + j) , 1:end );

        end
        
       
    end
    
    fprintf('%1.3f : ',pivot);
    fprintf('%1.3f\n',pivot_origin);
    
    if( i == 0)
        k1 = curr;
        yd_1 = curr_y;
        p_1 = curr_pr;
    end
    if( i == 1)
        k2 = curr;
        yd_2 = curr_y;
        p_2 = curr_pr;
    end
    if( i == 2)
        k3 = curr;
        yd_3 = curr_y;
        p_3 = curr_pr;
    end
    if( i == 3)
        k4 = curr;
        yd_4 = curr_y;
        p_4 = curr_pr;
    end
    if( i == 4)
        k5 = curr;
        yd_5 = curr_y;
        p_5 = curr_pr;
    end
    
end