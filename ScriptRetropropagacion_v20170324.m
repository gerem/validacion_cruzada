% ScriptRetropropagacion_v20170324
clc;
close all;
clear all;

% El valor es de 0's l?gicos se convierte a -1 por que se utiliza la
% funci?n tangente-hiperb?lica 

X_ = reshape( textread('dataset.txt', '%f'),40, 100 )';

%Salida deseada
Yd_ = [1,-1,-1,-1,-1;1,-1,-1,-1,-1;1,-1,-1,-1,-1;-1,-1,1,-1,-1;-1,-1,-1,-1,1;-1,-1,-1,-1,1;-1,-1,-1,-1,-1;-1,-1,-1,-1,-1;-1,-1,-1,-1,1;-1,-1,-1,-1,-1;-1,1,-1,-1,-1;-1,-1,-1,-1,-1;-1,-1,-1,-1,-1;-1,1,-1,-1,-1;-1,-1,-1,1,-1;-1,-1,-1,-1,1;-1,-1,-1,-1,-1;-1,1,-1,-1,-1;-1,-1,-1,1,-1;1,-1,-1,-1,-1;-1,-1,1,-1,-1;-1,-1,-1,-1,1;-1,-1,-1,1,-1;1,-1,-1,-1,-1;-1,-1,-1,1,-1;-1,-1,1,-1,-1;-1,1,-1,-1,-1;1,-1,-1,-1,-1;1,-1,-1,-1,-1;1,-1,-1,-1,-1;1,-1,-1,-1,-1;1,-1,-1,-1,-1;-1,1,-1,-1,-1;-1,1,-1,-1,-1;-1,-1,-1,-1,1;1,-1,-1,-1,-1;-1,-1,1,-1,-1;-1,-1,1,-1,-1;-1,-1,-1,-1,-1;-1,1,-1,-1,-1;-1,-1,1,-1,-1;-1,-1,1,-1,-1;-1,-1,-1,1,-1;-1,-1,-1,-1,1;-1,-1,-1,-1,1;-1,-1,1,-1,-1;-1,-1,-1,1,-1;-1,-1,-1,1,-1;-1,1,-1,-1,-1;-1,-1,-1,-1,1;-1,-1,-1,-1,-1;-1,-1,-1,-1,1;-1,-1,-1,1,-1;-1,-1,-1,-1,1;-1,-1,-1,1,-1;-1,-1,-1,-1,1;-1,1,-1,-1,-1;-1,-1,1,-1,-1;-1,-1,-1,1,-1;-1,-1,-1,-1,1;-1,-1,-1,-1,-1;1,-1,-1,-1,-1;-1,1,-1,-1,-1;-1,-1,-1,1,-1;-1,-1,-1,1,-1;-1,-1,-1,-1,1;1,-1,-1,-1,-1;1,-1,-1,-1,-1;-1,-1,-1,1,-1;-1,1,-1,-1,-1;-1,-1,1,-1,-1;-1,-1,-1,1,-1;-1,-1,-1,1,-1;-1,-1,1,-1,-1;-1,-1,1,-1,-1;-1,-1,-1,-1,1;-1,-1,1,-1,-1;-1,1,-1,-1,-1;-1,-1,-1,1,-1;-1,-1,1,-1,-1;1,-1,-1,-1,-1;-1,1,-1,-1,-1;-1,1,-1,-1,-1;1,-1,-1,-1,-1;-1,-1,1,-1,-1;-1,1,-1,-1,-1;-1,-1,-1,-1,1;-1,-1,1,-1,-1;-1,1,-1,-1,-1;-1,-1,1,-1,-1;-1,1,-1,-1,-1;-1,-1,1,-1,-1;1,-1,-1,-1,-1;1,-1,-1,-1,-1;-1,-1,-1,-1,-1;-1,1,-1,-1,-1;-1,-1,-1,1,-1;-1,-1,-1,-1,1;-1,-1,-1,-1,1;-1,-1,-1,1,-1];
    
%definicion de casos de prueba
k1   = [];
yd_1 = [];
p_1  = [];
py_1 = [];

k2   = [];
yd_2 = [];
p_2  = [];
py_2 = [];

k3   = [];
yd_3 = [];
p_3  = [];
py_3 = [];

k4   = [];
yd_4 = [];
p_4  = [];
py_4 = [];

k5   = [];
yd_5 = [];
p_5  = [];
py_5 = [];

elements = 100;
k = 5;

size_k = elements / k;
size_p = k - 1;

for i = 0:k - 1
    
    block = ( i * size_k );
    
    pivot = ( size_k * size_p ) - block;
    pivot_origin = elements - block;
    
    curr    =  X_( 1:pivot  , 1:end );
    curr_y  = Yd_( 1:pivot  , 1:end );
    
    lS = pivot + 1;
    lI = pivot_origin;
    
    fprintf('%1.3f <-> %1.3f\n',lS,lI);
    
    curr_pr  =  X_( lS:lI , 1:end );
    curr_p_y = Yd_( lS:lI , 1:end );
    
    if( pivot_origin ~= elements )
        
        for j = 1:block
        
            curr( (pivot + j) , 1:end ) = X_( (pivot_origin + j) , 1:end );
            curr_y( (pivot + j) , 1:end ) = Yd_( (pivot_origin + j) , 1:end );

        end
        
       
    end
    
    %fprintf('%1.3f : ',pivot);
    %fprintf('%1.3f\n',pivot_origin);
    
    if( i == 0)
        k1 = curr;
        yd_1 = curr_y;
        p_1 = curr_pr;
        py_1 = curr_p_y;
    end
    if( i == 1)
        k2 = curr;
        yd_2 = curr_y;
        p_2 = curr_pr;
        py_2 = curr_p_y;
    end
    if( i == 2)
        k3 = curr;
        yd_3 = curr_y;
        p_3 = curr_pr;
        py_3 = curr_p_y;
    end
    if( i == 3)
        k4 = curr;
        yd_4 = curr_y;
        p_4 = curr_pr;
        py_4 = curr_p_y;
    end
    if( i == 4)
        k5 = curr;
        yd_5 = curr_y;
        p_5 = curr_pr;
        py_5 = curr_p_y;
    end
    
end

X = k4';     %Vector de entrada -> Entrenamiento
Yd = yd_4';  %Salida deseada
P =  p_4;   %Vector de entrada -> Pruebas validaci?on

% 1 Capa de entrada (2 neuronas), 1 capa oculta (3 neuronas), 1 capa de
% salida (1 neurona)

%casos de prueba
%L = [1,2, 1]; %casos de prueba 1
%L = [1, 3, 1]; %casos de prueba 2
%L = [1, 4, 1, 1]; %casos de prueba 3
L = [ 40 , 14 , 5];

%datos de configuraci?n
eta     = 0.1;
alfa    = 0;
epsilon = 1e-3;

% Entrenamiento de la red
entrenamiento = Retropropagacion (X, Yd, L, eta, alfa, epsilon);
celldisp(entrenamiento.pesos);

iteraciones = linspace(1,entrenamiento.cont,entrenamiento.cont);

%Actualizaci?n para pesos de pruebas
X = P';

% Ejecuci?n de la red
% - Se agrega una columna para el bias
np = size (X, 2);
Xbias = [X; ones(1, np)];

%impresi?n de nombre del archivo
fprintf ('\nNombre de script: %s\n\n', mfilename);
fprintf ('\tY\n');

for i = 1:20
    fprintf ('\n');
    % - Se env?an los pesos ordenados y s?lo la(s)capa(s) oculta(s)
    test = Propagacion (Xbias(:,i), entrenamiento.pesos, entrenamiento.estructura( 2 ));
    
    %res = strtrim(sprintf('%d ',test))
    fprintf ('\t%1.3f',test);
end


return