function [O] = Propagacion (X, W, L)
% C�lculo de la propagaci�n
%	Descripci�n
%
%	  Propagacion calcula las salidas del perceptr�n multicapa a partir de
%	  sus entradas 
%
%	  FORWARDCOMP(X,W,L) recibe:
%	    X - matriz de los patrones de entrada incluyendo una fila de bias = 1.
%	    W - matriz de pesos de la red (cell vectors).
%	    L - vector de capas ocultas y neuronas por capa [m1 m2...mL].
%	  y devuelve
%       O - La salida de la red

    % Calcular el n�mero de salidas y de capas ocultas
    Q = size (W{end}, 1);   % Numero de Salidas
    H = size (L, 2);        % Numero de Capas Ocultas "Depth"
    % Inicializar los campos locales inducidos 'v'
    v = cell(1, H + 1);
    v{end} = zeros (Q, 1);
    for i = 1:H;
        v{i} = zeros (L(:, i), 1); 
    end
    % Inicializar las salidas 'y' de cada capa
    y = cell (1, H + 1);
    y{end} = zeros(Q, 1);
    for i = 1:H;
        y{i} = [zeros(L(:,i),1);1];
    end
    for j = 1:(H + 1)
        if j == 1
            v{j} = W{j} * X;                % C�lculo de u
            %y{j}(1:end-1) = logsig (v{j});  % C�lculo de y
            y{j}(1:end-1) = tansig (v{j});  % C�lculo de y
        elseif j == (H + 1)
            v{j} = W{j} * y{j-1};           % C�lculo de u
            y{j} = v{j};           % C�lculo de y de la capa de salida
            O = y{j};
        else
            v{j} = W{j} * y{j-1};           % C�lculo de u
            %y{j}(1:end-1) = logsig (v{j});  % C�lculo de y
            y{j}(1:end-1) = tansig (v{j});  % C�lculo de y
        end
    end