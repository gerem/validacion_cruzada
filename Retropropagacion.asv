% Algoritmo de entrenamiento de retropropagaci�n
% X: Vector de entradas + bias
% Yd: Vector de salidas deseadas
% L: Capas
% eta: tasa de aprendizaje
% alfa: constante de momento
% epsilon: tolerancia
function entrenamiento = Retropropagacion (X, Yd, L, eta, alfa, epsilon)
    
    %% 1. Inicializaci�n y c�lculo de par�metros
    % Par�metros del algoritmo
    
    mse   = Inf;% Asumiendo pesos iniciales no adecuados
    epoca = 1;
    cont = 0;
    
    evolucionError = []; %Acumulamos los errores con la norma
    vectorPesosFinal = []; %Vector de pesos final
    
    % Se calculan los tama�os respectivos de las matrices
    [N, P]  = size (X);         % Numero de patrones P y entradas N
    [Q, Pd] = size (Yd);        % Numero de patrones P y salidas Q 
    ncapas   = length (L);      % Profundidad de la red - n�mero de capas
    
    % Inicializar matriz de pesos para cada capa en el rango [-1,1]
    W = cell (1, ncapas - 1);   % Pre-alocacion de la matriz de pesos
    
    for m = 1:(ncapas - 2)
        W{1, m} = -1+2*rand(L(1,m+1),L(1,m)+1); % Agrega un valor adicional para el bias
       %celldisp(W);
    end
    
    W{end} = -1+2*rand(L(end),L(end-1)+1); % Agrega un valor adicional para el bias
    
    % Inicializar matrices de los delta-pesos de ajuste
    dW = cell (1, ncapas - 1);   % Pre-alocacion de los delta pesos
    
    for m = 1:(ncapas - 1)
        dW{m} = zeros (size (W{m}));
    end
    
    % Inicializar los campos locales inducidos 'v'
    v = cell(1, ncapas);         % Pre-alocacion de los campos locales
    
    for i = 1:(ncapas - 1);
        v{i} = [zeros(L(i),1); 1]; % Inicializa a 1 el bias
    end
    
    v{end} = zeros (L(end), 1);
    % Inicializar las salidas 'y' de cada capa
    y = cell(1, ncapas - 1);     % Pre-alocacion de las salidas locales
    for i = 1:(ncapas - 2);
        y{i} = zeros (L(i + 1), 1);
    end
    y{end} = zeros (L(end), 1);
    
    %% 2. C�lculo Forward y Backward para cada epoca
    while (mse > epsilon) && (epoca <= 10000)
        
        e = zeros(Q, P);
        err = zeros(1, P);
        
        % Para cada patr�n "p"
        
        for p = 1:P
            %% Propagaci�n de la se�al
            % C�lculo Forward capa-por-capa para cada patr�n "p"
            % Asignaci�n del patr�n "p" a la entrada de la red
            v{1}(1:end-1) = X(:,p);
            % Para cada capa oculta y para la capa de salida
            for i = 1:(ncapas - 1)
                y{i} = W{i} * v{i}; % C�lculo de la sumatoria
                % C�lculo de la salida de la capa "i" y a la vez entrada para la
                % capa i+1
                if i < ncapas-1
                    % No modifica el valor del bias
                    v{i+1}(1:end-1) = tansig(y{i});
                else
                    % No hay bias en la salida de la capa de salida
                    %v{i+1} = tansig(y{i});
                    v{i+1} = y{i};
                end
            end
            
            % Se�al de error
            e(:,p) = Yd(:,p) - v{end};
            
            % Energ�a del error
            if size (Yd,1) == 1
                % Salidas escalares
                err(1,p) = 0.5 * (e(:,p).^2);
            elseif size(Yd,1) > 1
                % Salidas vectoriales
                err(1,p) = 0.5 * sum(e(:,p).^2);
            end
            %% Retropropagaci�n del error
            % C�lculo Backward capa-por-capa para cada patr�n "p"
            delta = e(:,p);
            %(tansig ('dn', y{end}));%Llama la derivada
            % Ajuste de pesos
            for i = ncapas-1:-1:1                 
                dW{i} = eta * delta * v{i}' + alfa.*dW{i};
                W{i} = W{i} + dW{i};
                if i > 1
                    delta = tansig('dn',y{i-1}).*(delta'*W{i}(:,1:end-1))'; % Llama la derivada
                end
            end
        end
        
        % C�lculo del error cuadratico medio
        mse = (1/P) * sum(err);
        vectorPesosFinal = err;
        evolucionError(end + 1) = mse; 

        epoca = epoca + 1;
        
        if (0) %<------ NOTA: establecer a 1 si desean ver la gr�fica
            %figure(1);
            %hold on
            %semilogx(epoca, mse, 'r')
            %plot (epoca, mse, 'r')
            %hold off
            %fprintf('\t-> %1.3f\n',  mse);
        end
       cont= cont+1;
    end
    
    % Informaci�n al t�rmino del entrenamiento
    entrenamiento.pesos      = W;       % Matriz de pesos
    entrenamiento.cont       = cont;    % N�mero de epocas alcanzado
    entrenamiento.epocas     = epoca;   % N�mero de epocas alcanzado
    entrenamiento.estructura = L;       % Capas (mismo que los par�metros)
    entrenamiento.error      = mse;     % Error cuadr�tico medio observado
    entrenamiento.vectorPesosFinal = vectorPesosFinal;
    entrenamiento.evolucionError   = evolucionError;
    
end